package com.test.project.drones.job;

import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.service.DronesService;
import com.test.project.drones.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class BatteryLevelJob {

    @Autowired
    DronesService dronesService;

    @Autowired
    HistoryService historyService;

    @Scheduled(fixedDelayString = "${job.scheduler.delay}")
    public void checkBatteryLevel() {
        for (Drone drone: dronesService.getAllDrones()) {
            historyService.createHistoryRecord(drone);

            //decrease BatteryLevel for emulation
            dronesService.decreaseBatteryLevel(drone.getBattery());
        }
    }
}
