package com.test.project.drones.converter.impl;

import com.test.project.drones.converter.DroneConverter;
import com.test.project.drones.dto.BatteryHistoryDto;
import com.test.project.drones.dto.DroneDto;
import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.history.BatteryHistory;
import com.test.project.drones.service.DronesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DroneConverterImpl implements DroneConverter {

    @Autowired
    DronesService dronesService;

    @Override
    public DroneDto mapToDroneDto(Drone drone) {
        DroneDto dto = new DroneDto(drone.getSerialNumber(), drone.getModel().getName());
        dto.setState(drone.getState());
        dto.setBatteryLevel(drone.getBatteryLevel());
        dto.setWeightLimit(drone.getWeightLimit());
        dto.setWeightLoaded(dronesService.getLoadedWeight(drone));

        return dto;
    }

    @Override
    public Drone mapToDroneEntity(DroneDto dto) {
        return new Drone(dto.getSerialNumber(), dto.getModel());
    }

    @Override
    public BatteryHistoryDto mapToBatteryHistoryDto(BatteryHistory history) {
        return new BatteryHistoryDto(history.getDate(), history.getBatteryLevel());
    }

    @Override
    public List<BatteryHistoryDto> mapToBatteryHistoriesDto(List<BatteryHistory> histories) {
        return histories.stream().map(this::mapToBatteryHistoryDto).collect(Collectors.toList());
    }
}
