package com.test.project.drones.converter.impl;

import com.test.project.drones.converter.DroneConverter;
import com.test.project.drones.converter.OrderConverter;
import com.test.project.drones.dto.OrderDto;
import com.test.project.drones.dto.OrderItemDto;
import com.test.project.drones.model.medication.Medication;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.model.order.OrderItem;
import com.test.project.drones.service.OrderService;
import com.test.project.drones.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderConverterImpl implements OrderConverter {

    @Autowired
    DroneConverter droneConverter;

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    OrderService orderService;

    @Override
    public OrderDto mapToOrderDto(Order order) {
        OrderDto dto = new OrderDto(order.getId(), order.getDeliveryDate(), order.getDeliveryAddress());
        dto.setOrderDate(order.getOrderDate());
        dto.setWeightSum(orderService.getOrderWeight(order));

        if (order.getDrone() != null) {
            dto.setDrone(droneConverter.mapToDroneDto(order.getDrone()));
        }

        if (order.getOrderItems() != null && !order.getOrderItems().isEmpty()) {
            dto.setOrderItems(mapToOrderItemsDto(order.getOrderItems()));
        }

        return dto;
    }

    @Override
    public Order mapToOrderEntity(OrderDto dto) {
        Order order = new Order();
        order.setDeliveryDate(dto.getDeliveryDate());
        order.setDeliveryAddress(dto.getDeliveryAddress());

        if (dto.getDrone() != null) {
            order.setDrone(droneConverter.mapToDroneEntity(dto.getDrone()));
        }

        return order;
    }

    @Override
    public List<OrderItemDto> mapToOrderItemsDto(List<OrderItem> orderItems) {
        Map<Medication, Integer> map = new HashMap<>();

        for (OrderItem item : orderItems) {
            Medication key = item.getMedicationItem().getMedication();
            map.put(key, map.containsKey(key) ? map.get(key) + 1 : 1);

        }
        List<OrderItemDto> dtos = new ArrayList<>();
        for (Map.Entry<Medication, Integer> entry : map.entrySet()) {
            Medication medication = entry.getKey();
            OrderItemDto dto = new OrderItemDto(medication.getCode(), medication.getName());
            dto.setQuantity(entry.getValue());
            dtos.add(dto);
        }

        return dtos;
    }
}
