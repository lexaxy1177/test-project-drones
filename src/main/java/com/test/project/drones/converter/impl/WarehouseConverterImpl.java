package com.test.project.drones.converter.impl;

import com.test.project.drones.converter.WarehouseConverter;
import com.test.project.drones.dto.MedicationDto;
import com.test.project.drones.model.medication.Medication;
import com.test.project.drones.model.medication.MedicationItem;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WarehouseConverterImpl implements WarehouseConverter {

    @Override
    public List<MedicationDto> mapToMedicationDto(List<MedicationItem> items) {
        Map<Medication, Integer> map = new HashMap<>();
        for (MedicationItem item : items) {
            Medication key = item.getMedication();
            map.put(key, map.containsKey(key) ? map.get(key) + 1 : 1);

        }
        List<MedicationDto> dtos = new ArrayList<>();
        for (Map.Entry<Medication, Integer> entry : map.entrySet()) {
            MedicationDto dto = new MedicationDto();
            Medication medication = entry.getKey();
            dto.setCode(medication.getCode());
            dto.setName(medication.getName());
            dto.setWeight(medication.getWeight());
            dto.setAvailableQuantity(entry.getValue());
            dtos.add(dto);
        }

        return dtos;
    }
}
