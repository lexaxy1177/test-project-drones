package com.test.project.drones.converter;

import com.test.project.drones.dto.BatteryHistoryDto;
import com.test.project.drones.dto.DroneDto;
import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.history.BatteryHistory;

import java.util.List;

public interface DroneConverter {

    DroneDto mapToDroneDto(Drone drone);

    Drone mapToDroneEntity(DroneDto dto);

    BatteryHistoryDto mapToBatteryHistoryDto(BatteryHistory history);

    List<BatteryHistoryDto> mapToBatteryHistoriesDto(List<BatteryHistory> orderItems);
}
