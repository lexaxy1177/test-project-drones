package com.test.project.drones.converter;

import com.test.project.drones.dto.OrderDto;
import com.test.project.drones.dto.OrderItemDto;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.model.order.OrderItem;

import java.util.List;

public interface OrderConverter {

    OrderDto mapToOrderDto(Order order);

    Order mapToOrderEntity(OrderDto dto);

    List<OrderItemDto> mapToOrderItemsDto(List<OrderItem> orderItems);
}
