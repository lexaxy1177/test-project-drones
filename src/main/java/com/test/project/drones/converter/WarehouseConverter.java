package com.test.project.drones.converter;

import com.test.project.drones.dto.MedicationDto;
import com.test.project.drones.model.medication.MedicationItem;

import java.util.List;

public interface WarehouseConverter {

    List<MedicationDto> mapToMedicationDto(List<MedicationItem> items);
}
