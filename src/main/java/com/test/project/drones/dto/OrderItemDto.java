package com.test.project.drones.dto;

import lombok.Data;

@Data
public class OrderItemDto {

    private String code;
    private String name;
    private int quantity;

    public OrderItemDto(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
