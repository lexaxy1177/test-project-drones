package com.test.project.drones.dto;

import lombok.Data;

@Data
public class MedicationDto {

    private String name;
    private String code;
    private int availableQuantity;
    private int weight;
}
