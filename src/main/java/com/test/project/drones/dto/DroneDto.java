package com.test.project.drones.dto;

import com.test.project.drones.model.drone.StateDrone;
import lombok.Data;
import lombok.NonNull;

@Data
public class DroneDto {

    @NonNull
    private String serialNumber;
    @NonNull
    private String model;

    private int batteryLevel;
    private StateDrone state;
    private int weightLimit;
    private int weightLoaded;

    public DroneDto(@NonNull String serialNumber, @NonNull String model) {
        this.serialNumber = serialNumber;
        this.model = model;
    }
}
