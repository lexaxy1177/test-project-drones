package com.test.project.drones.dto;

import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
public class BatteryHistoryDto {

    @NonNull
    private Date date;

    @NonNull
    private int level;

    public BatteryHistoryDto(@NonNull Date date, @NonNull int level) {
        this.date = date;
        this.level = level;
    }
}
