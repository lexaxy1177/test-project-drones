package com.test.project.drones.dto;

import lombok.Data;
import lombok.NonNull;

import java.util.Date;
import java.util.List;

@Data
public class OrderDto {

    @NonNull
    private Date deliveryDate;
    @NonNull
    private String deliveryAddress;
    private long orderId;
    private Date orderDate;
    private int weightSum;
    private DroneDto drone;
    private List<OrderItemDto> orderItems;

    public OrderDto(long orderId, @NonNull Date deliveryDate, @NonNull String deliveryAddress) {
        this.orderId = orderId;
        this.deliveryDate = deliveryDate;
        this.deliveryAddress = deliveryAddress;
    }
}
