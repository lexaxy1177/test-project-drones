package com.test.project.drones.controller;

import com.test.project.drones.converter.OrderConverter;
import com.test.project.drones.dto.OrderDto;
import com.test.project.drones.dto.OrderItemDto;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.service.OrderItemService;
import com.test.project.drones.service.OrderService;
import com.test.project.drones.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {

    @Autowired
    OrderService orderService;

    @Autowired
    OrderConverter orderConverter;

    @Autowired
    OrderItemService orderItemService;

    @Autowired
    ValidationService validationService;

    @PostMapping()
    public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto dto) {
        Order requestOrder = orderConverter.mapToOrderEntity(dto);
        Order order = orderService.createOrder(requestOrder);

        return new ResponseEntity<>(orderConverter.mapToOrderDto(order), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDto> getOrderById(@PathVariable long id) {
        Order order = orderService.getOrderById(id);

        return new ResponseEntity<>(orderConverter.mapToOrderDto(order), HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        List<Order> drones = orderService.getAllOrders();
        if (drones.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<OrderDto> response = drones.stream().map(orderConverter::mapToOrderDto).collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{id}/assign/{serialNumber}")
    public ResponseEntity<HttpStatus> assignDrone(@PathVariable long id,
                                                  @PathVariable String serialNumber) {
        orderService.assignDrone(id, serialNumber);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("{id}/addMedication")
    public ResponseEntity<OrderDto> addMedication(@PathVariable long id,
                                                  @RequestBody List<OrderItemDto> itemsDto) {
        Order order = orderService.getOrderById(id);
        validationService.checkWeightLimitBeforeAdd(order, itemsDto);
        for (OrderItemDto dto : itemsDto) {
            orderItemService.createAndAddOrderItemsToOrder(order, dto);
        }
        return new ResponseEntity<>(orderConverter.mapToOrderDto(orderService.getOrderById(id)), HttpStatus.CREATED);
    }

    @PutMapping("{id}/complete")
    public ResponseEntity<HttpStatus> completeOrder(@PathVariable long id) {
        orderService.completeOrder(orderService.getOrderById(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("{id}/delivery")
    public ResponseEntity<HttpStatus> deliveryOrder(@PathVariable long id) {
        orderService.deliveryOrder(orderService.getOrderById(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("{id}/cancel")
    public ResponseEntity<HttpStatus> cancelOrder(@PathVariable long id) {
        orderService.cancelOrder(orderService.getOrderById(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
