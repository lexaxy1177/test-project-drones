package com.test.project.drones.controller;

import com.test.project.drones.converter.DroneConverter;
import com.test.project.drones.dto.BatteryHistoryDto;
import com.test.project.drones.dto.DroneDto;
import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.history.BatteryHistory;
import com.test.project.drones.service.DronesService;
import com.test.project.drones.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/drones")
public class DronesController {

    @Autowired
    DronesService dronesService;

    @Autowired
    DroneConverter droneConverter;

    @Autowired
    HistoryService historyService;

    @GetMapping("/{serialNumber}")
    public ResponseEntity<DroneDto> getDroneBySerialNumber(@PathVariable("serialNumber") String serialNumber) {
        Drone drone = dronesService.getDroneBySerialNumber(serialNumber);
        return new ResponseEntity<>(droneConverter.mapToDroneDto(drone), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<DroneDto> createDrone(@RequestBody DroneDto dto) {
        Drone requestDrone = droneConverter.mapToDroneEntity(dto);
        Drone newDrone = dronesService.createDrone(requestDrone);

        return new ResponseEntity<>(droneConverter.mapToDroneDto(newDrone), HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<DroneDto>> getAllDrones() {
        List<Drone> drones = dronesService.getAllDrones();
        if (drones.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<DroneDto> response = drones.stream().map(droneConverter::mapToDroneDto).collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{serialNumber}")
    public ResponseEntity<HttpStatus> deleteDroneById(@PathVariable("serialNumber") String serialNumber) {
        dronesService.deleteDrone(serialNumber);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("charging/{serialNumber}")
    public ResponseEntity<HttpStatus> releaseDroneById(@PathVariable("serialNumber") String serialNumber) {
        Drone drone = dronesService.getDroneBySerialNumber(serialNumber);
        dronesService.sendForCharging(drone);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/free")
    public ResponseEntity<List<DroneDto>> getFreeDrones() {
        List<Drone> drones = dronesService.getFreeDrones();
        if (drones.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<DroneDto> response = drones.stream().map(droneConverter::mapToDroneDto).collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/history/battery/{serialNumber}")
    public ResponseEntity<List<BatteryHistoryDto>> getBatteryLevelHistory(@PathVariable("serialNumber") String serialNumber) {
        Drone drone = dronesService.getDroneBySerialNumber(serialNumber);
        List<BatteryHistory> histories = historyService.getBatteryLevelHistory(drone);

        return new ResponseEntity<>(droneConverter.mapToBatteryHistoriesDto(histories), HttpStatus.OK);
    }

}
