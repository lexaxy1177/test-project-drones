package com.test.project.drones.controller;

import com.test.project.drones.converter.WarehouseConverter;
import com.test.project.drones.dto.MedicationDto;
import com.test.project.drones.model.medication.Medication;
import com.test.project.drones.model.medication.MedicationItem;
import com.test.project.drones.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    WarehouseConverter warehouseConverter;

    @GetMapping(value = "/image/{code}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImageForMedication(@PathVariable("code") String code) {
        Medication medication = warehouseService.getMedicationByCode(code);
        byte[] imageBytes = warehouseService.getImageForMedication(medication);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(imageBytes);
    }

    @GetMapping(value = "/available")
    public ResponseEntity<List<MedicationDto>> getAvailableMedication() {
        List<MedicationItem> medications = warehouseService.getAvailableMedication();
        if (medications.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<MedicationDto> medicationDtos = warehouseConverter.mapToMedicationDto(medications);
        return new ResponseEntity<>(medicationDtos, HttpStatus.OK);
    }
}
