package com.test.project.drones;

import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.drone.ModelDrone;
import com.test.project.drones.model.medication.Medication;
import com.test.project.drones.model.medication.MedicationItem;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.service.DronesService;
import com.test.project.drones.service.OrderItemService;
import com.test.project.drones.service.OrderService;
import com.test.project.drones.service.WarehouseService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Component
public class InitTestLoader {
    @Autowired
    DronesService dronesService;

    @Autowired
    OrderItemService orderItemService;

    @Autowired
    OrderService orderService;

    @Autowired
    WarehouseService warehouseService;

    @PostConstruct
    void prepareTestData() throws NotFoundException {
        createTestWarehouse();
        prepareTestDrones();
        prepareTestOrders();
    }

    void createTestWarehouse() {
        LocalDateTime nowLocal = LocalDateTime.now();
        Date dateNow = Date.from(nowLocal.atZone(ZoneId.systemDefault()).toInstant());
        LocalDateTime plusYearsLocal = nowLocal.plusYears(1);
        Date datePlusYears = Date.from(plusYearsLocal.atZone(ZoneId.systemDefault()).toInstant());

        Medication medication = new Medication("MEDICATION1", "Analgin", 2);
        medication.setImgPath("image/analgin.jpg");
        Medication medicationRes = warehouseService.createMedication(medication);
        warehouseService.addMedicationToWarehouse(medicationRes, dateNow, datePlusYears, 100);

        Medication medication2 = new Medication("MEDICATION2", "Paracetamol", 3);
        medication2.setImgPath("image/paracetamol.jpg");
        Medication medicationRes2 = warehouseService.createMedication(medication2);
        warehouseService.addMedicationToWarehouse(medicationRes2, dateNow, datePlusYears, 100);

        Medication medication3 = new Medication("MEDICATION3", "Aspirin", 3);
        medication3.setImgPath("image/aspirin.jpg");
        Medication medicationRes3 = warehouseService.createMedication(medication3);
        warehouseService.addMedicationToWarehouse(medicationRes3, dateNow, datePlusYears, 200);

        Medication medication4 = new Medication("MEDICATION4", "Strepsils", 300);
        medication4.setImgPath("image/strepsils.jpg");
        Medication medicationRes4 = warehouseService.createMedication(medication4);
        warehouseService.addMedicationToWarehouse(medicationRes4, dateNow, datePlusYears, 200);
    }

    void prepareTestOrders() {
        Order order = new Order(new Date(), "Test address delivery");
        Order orderRes = orderService.createOrder(order);
        List<MedicationItem> medicationItems = warehouseService.reserveMedicationItem("MEDICATION1", 3);
        List<MedicationItem> medicationItems2 = warehouseService.reserveMedicationItem("MEDICATION2", 5);
        orderItemService.createOrderItems(orderRes, medicationItems);
        orderItemService.createOrderItems(orderRes, medicationItems2);
        orderService.assignDrone(orderRes, dronesService.getDroneBySerialNumber("DRON1"));


        Order order2 = new Order(new Date(), "Test address delivery2");
        Order orderRes2 = orderService.createOrder(order2);
        List<MedicationItem> medicationItems3 = warehouseService.reserveMedicationItem("MEDICATION2", 10);
        List<MedicationItem> medicationItems4 = warehouseService.reserveMedicationItem("MEDICATION3", 20);
        orderItemService.createOrderItems(orderRes2, medicationItems3);
        orderItemService.createOrderItems(orderRes2, medicationItems4);
        orderService.assignDrone(orderRes2, dronesService.getDroneBySerialNumber("DRON2"));
    }

    void prepareTestDrones() {
        dronesService.createDrone(new Drone("DRON1", ModelDrone.LIGHTWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON2", ModelDrone.HEAVYWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON3", ModelDrone.CRUISERWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON4", ModelDrone.CRUISERWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON5", ModelDrone.LIGHTWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON6", ModelDrone.CRUISERWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON7", ModelDrone.MIDDLEWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON8", ModelDrone.HEAVYWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON9", ModelDrone.MIDDLEWEIGHT.getName()));
        dronesService.createDrone(new Drone("DRON10", ModelDrone.LIGHTWEIGHT.getName()));
    }
}
