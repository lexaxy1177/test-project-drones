package com.test.project.drones.service;

import com.test.project.drones.dto.OrderItemDto;
import com.test.project.drones.model.medication.MedicationItem;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.model.order.OrderItem;

import java.util.List;

public interface OrderItemService {

    List<OrderItem> createAndAddOrderItemsToOrder(Order order, OrderItemDto dto);

    List<OrderItem> createOrderItems(Order order, List<MedicationItem> items);

    OrderItem createOrderItem(Order order, MedicationItem item);

    void cancelOrderItem(OrderItem item);
}
