package com.test.project.drones.service.impl;

import com.test.project.drones.model.DroneConstants;
import com.test.project.drones.model.drone.Battery;
import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.drone.StateDrone;
import com.test.project.drones.repository.BatteryRepository;
import com.test.project.drones.repository.DronesRepository;
import com.test.project.drones.service.DronesService;
import com.test.project.drones.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DronesServiceImpl implements DronesService {

    @Autowired
    DronesRepository dronesRepository;

    @Autowired
    BatteryRepository batteryRepository;

    @Autowired
    OrderService orderService;

    @Override
    @Transactional
    public Drone createDrone(Drone drone) {
        if (dronesRepository.existsById(drone.getSerialNumber())) {
            throw new RuntimeException("Drone already exists with SerialNumber: " + drone.getSerialNumber());
        }

        return dronesRepository.save(new Drone(drone.getSerialNumber(), drone.getModel().getName()));
    }

    @Override
    public void changeStatus(Drone drone, StateDrone state) {
        drone.setState(state);
        dronesRepository.flush();;
    }

    @Override
    public Drone getDroneBySerialNumber(String serialNumber) {
        Optional<Drone> drone = dronesRepository.findById(serialNumber);

        if (!drone.isPresent()) {
            throw new RuntimeException("Drone with serialNumber = " + serialNumber + " was not found.");
        }
        return drone.get();
    }

    @Override
    public List<Drone> getAllDrones() {
        return dronesRepository.findAll();
    }

    @Override
    public void deleteDrone(String serialNumber) {
        dronesRepository.deleteById(serialNumber);
    }

    @Override
    public List<Drone> getFreeDrones() {
        return dronesRepository.findAll().stream()
                .filter(drone -> StateDrone.IDLE.equals(drone.getState()))
                .filter(drone -> drone.getBatteryLevel() > DroneConstants.MIN_BATTERY_LEVEL)
                .collect(Collectors.toList());
    }

    @Override
    public int getLoadedWeight(Drone drone) {
        if (drone.getOrder() == null || drone.getOrder().getOrderItems().isEmpty()) {
            return 0;
        }
        return orderService.getOrderWeight(drone.getOrder());
    }

    @Override
    public void sendForCharging(Drone drone) {
        if (drone.getOrder() != null) {
            throw new RuntimeException("Drone under loading need to find replacement");
        }

        drone.getBattery().setCapacity(100);
        batteryRepository.flush();
    }

    @Override
    public void decreaseBatteryLevel(Battery battery) {
        if (battery.getCapacity() > 0) {
            battery.setCapacity(battery.getCapacity() - 5);
            batteryRepository.save(battery);
        }
    }
}
