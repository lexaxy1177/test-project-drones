package com.test.project.drones.service.impl;

import com.test.project.drones.model.DroneConstants;
import com.test.project.drones.model.medication.Medication;
import com.test.project.drones.model.medication.MedicationItem;
import com.test.project.drones.model.medication.MedicationStatus;
import com.test.project.drones.repository.MedicationItemRepository;
import com.test.project.drones.repository.MedicationRepository;
import com.test.project.drones.service.ValidationService;
import com.test.project.drones.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    MedicationRepository medicationRepository;

    @Autowired
    MedicationItemRepository medicationItemRepository;

    @Autowired
    ValidationService validationService;


    @Override
    public Medication getMedicationByCode(String code) {
        Optional<Medication> medication = medicationRepository.findById(code);
        if (!medication.isPresent()) {
            throw new RuntimeException("Medication with code" + code + "was not found.");
        }
        return medication.get();
    }

    @Override
    public List<MedicationItem> getAvailableMedication() {
        return medicationItemRepository.findAll().stream()
                .filter(item -> MedicationStatus.AVAILABLE.equals(item.getStatus()))
                .filter(item -> item.getOrderItem() == null)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<MedicationItem> reserveMedicationItem(String code, int quantity) {
        Medication medication = getMedicationByCode(code);

        List<MedicationItem> reservedItems = medication.getMedicationItems().stream()
                .filter(item -> MedicationStatus.AVAILABLE.equals(item.getStatus()))
                .limit(quantity)
                .peek(item -> item.setStatus(MedicationStatus.RESERVED))
                .collect(Collectors.toList());
        medicationRepository.flush();

        return reservedItems;
    }

    @Override
    public MedicationItem addMedicationToWarehouse(Medication medication, Date issueDate, Date expiryDate) {
        MedicationItem medicationItem = new MedicationItem(medication, issueDate, expiryDate);
        return medicationItemRepository.save(medicationItem);
    }

    @Override
    public List<MedicationItem> addMedicationToWarehouse(Medication medication, Date issueDate, Date expiryDate, int quantity) {
        List<MedicationItem> medicationItems = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            medicationItems.add(addMedicationToWarehouse(medication, issueDate, expiryDate));
        }

        return medicationItems;
    }

    @Override
    public byte[] getImageForMedication(Medication medication) {
        if (medication.getImgPath() == null) {
            throw new RuntimeException("Image not exists for Medication with code = " + medication.getCode());
        }
        ClassPathResource imgFile = new ClassPathResource(medication.getImgPath());
        try {
            return StreamUtils.copyToByteArray(imgFile.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException("Problem with downloading image for code: " + medication.getCode());
        }
    }

    @Override
    public Medication createMedication(Medication medication) {
        validationService.checkFormatOfInputValue(medication.getCode(), DroneConstants.MEDICATION_CODE_PATTERNS);
        validationService.checkFormatOfInputValue(medication.getName(), DroneConstants.MEDICATION_NAME_PATTERNS);

        return medicationRepository.save(medication);
    }

    @Override
    public void releaseMedicationItem(MedicationItem medicationItem) {
        medicationItem.setStatus(MedicationStatus.AVAILABLE);
        medicationItemRepository.flush();
    }
}
