package com.test.project.drones.service;

import com.test.project.drones.model.medication.Medication;
import com.test.project.drones.model.medication.MedicationItem;

import java.util.Date;
import java.util.List;

public interface WarehouseService {

    Medication getMedicationByCode(String code);

    List<MedicationItem> getAvailableMedication();

    List<MedicationItem> reserveMedicationItem(String code, int quantity);

    MedicationItem addMedicationToWarehouse(Medication medication, Date issueDate, Date expiryDate);

    List<MedicationItem> addMedicationToWarehouse(Medication medication, Date issueDate, Date expiryDate, int quantity);

    byte[] getImageForMedication(Medication medication);

    Medication createMedication(Medication medication);

    void releaseMedicationItem(MedicationItem medicationItem);
}
