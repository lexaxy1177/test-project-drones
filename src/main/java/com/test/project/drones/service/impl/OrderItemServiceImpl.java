package com.test.project.drones.service.impl;

import com.test.project.drones.dto.OrderItemDto;
import com.test.project.drones.model.medication.MedicationItem;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.model.order.OrderItem;
import com.test.project.drones.repository.OrderItemRepository;
import com.test.project.drones.service.OrderItemService;
import com.test.project.drones.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderItemServiceImpl implements OrderItemService {

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    OrderItemRepository orderItemRepository;

    @Override
    @Transactional
    public List<OrderItem> createAndAddOrderItemsToOrder(Order order, OrderItemDto dto) {
        List<MedicationItem> medicationItems = warehouseService.reserveMedicationItem(dto.getCode(), dto.getQuantity());

        return createOrderItems(order, medicationItems) ;
    }

    @Override
    public List<OrderItem> createOrderItems(Order order, List<MedicationItem> items) {
        List<OrderItem> orderItems = new ArrayList<>();
        for (MedicationItem item: items) {
            orderItems.add(createOrderItem(order, item));
        };

        return orderItems;
    }

    @Override
    public OrderItem createOrderItem(Order order, MedicationItem item) {
        return orderItemRepository.save(new OrderItem(order, item));
    }

    @Override
    public void cancelOrderItem(OrderItem item) {
        warehouseService.releaseMedicationItem(item.getMedicationItem());
        orderItemRepository.delete(item);
    }
}
