package com.test.project.drones.service.impl;

import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.drone.StateDrone;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.model.order.OrderItem;
import com.test.project.drones.model.order.OrderStatus;
import com.test.project.drones.repository.OrderRepository;
import com.test.project.drones.service.DronesService;
import com.test.project.drones.service.OrderItemService;
import com.test.project.drones.service.OrderService;
import com.test.project.drones.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    DronesService dronesService;

    @Autowired
    ValidationService validationService;

    @Autowired
    OrderItemService orderItemService;

    @Override
    public Order getOrderById(long id) {
        Optional<Order> order = orderRepository.findById(id);

        if (!order.isPresent()) {
            throw new RuntimeException("Order with id = " + id + " was not found.");
        }
        return order.get();
    }

    @Override
    public Order createOrder(Order order) {
        Order orderRes = new Order(order.getDeliveryDate(), order.getDeliveryAddress());
        if (order.getDrone() != null) {
            orderRes.setDrone(order.getDrone());
        }

        return orderRepository.save(orderRes);
    }

    @Override
    public void assignDrone(long orderId, String serialNumber) {
        Drone drone = dronesService.getDroneBySerialNumber(serialNumber);
        Order order = getOrderById(orderId);
        assignDrone(order, drone);
    }

    @Override
    public void assignDrone(Order order, Drone drone) {
        validationService.checkAvailableToLoad(drone);
        validationService.checkDroneWeightLimit(drone, order);

        if (order.getDrone() != null) {
            Drone oldDrone = order.getDrone();
            dronesService.changeStatus(oldDrone, StateDrone.IDLE);
        }

        dronesService.changeStatus(drone, StateDrone.LOADING);
        order.setDrone(drone);
        order.setStatus(OrderStatus.DRONE_ASSIGNED);
        orderRepository.save(order);
    }

    @Override
    public void addOrderItems(Order order, List<OrderItem> orderItems) {
        order.getOrderItems().addAll(orderItems);
        orderRepository.flush();
    }

    @Override
    public void completeOrder(Order order) {
        order.setStatus(OrderStatus.COMPLETED);
        order.getDrone().setState(StateDrone.LOADED);
        orderRepository.flush();
    }

    @Override
    public void deliveryOrder(Order order) {
        order.setStatus(OrderStatus.IN_DELIVERY);
        order.getDrone().setState(StateDrone.DELIVERING);
        orderRepository.flush();
    }

    @Override
    public int getOrderWeight(Order order) {
        if (order.getOrderItems() == null || order.getOrderItems().isEmpty()) {
            return 0;
        }
        return order.getOrderItems().stream()
                .filter(Objects::nonNull)
                .map(orderItem -> orderItem.getMedicationItem().getMedication().getWeight())
                .mapToInt(weigh -> weigh)
                .sum();
    }

    @Override
    @Transactional
    public void cancelOrder(Order order) {
        dronesService.changeStatus(order.getDrone(), StateDrone.IDLE);
        order.setStatus(OrderStatus.CANCELED);
        order.setDrone(null);
        orderRepository.flush();

        order.getOrderItems().stream()
                .filter(Objects::nonNull)
                .forEach(item -> orderItemService.cancelOrderItem(item));
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
}
