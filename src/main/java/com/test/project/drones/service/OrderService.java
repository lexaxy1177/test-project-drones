package com.test.project.drones.service;

import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.model.order.OrderItem;

import java.util.List;

public interface OrderService {

    Order getOrderById(long id);

    Order createOrder(Order order);

    void assignDrone(long orderId, String serialNumber);

    void assignDrone(Order order, Drone drone);

    void addOrderItems(Order order, List<OrderItem> orderItems);

    void completeOrder(Order order);

    void deliveryOrder(Order order);

    int getOrderWeight(Order order);

    void cancelOrder(Order order);

    List<Order> getAllOrders();
}
