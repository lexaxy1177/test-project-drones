package com.test.project.drones.service.impl;

import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.history.BatteryHistory;
import com.test.project.drones.repository.BatteryHistoryRepository;
import com.test.project.drones.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    BatteryHistoryRepository historyRepository;

    @Override
    public void createHistoryRecord(Drone drone) {
        historyRepository.save(new BatteryHistory(drone));
    }

    @Override
    public List<BatteryHistory> getBatteryLevelHistory(Drone drone) {
        return drone.getBatteryHistory();
    }
}
