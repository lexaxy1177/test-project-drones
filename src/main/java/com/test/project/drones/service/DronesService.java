package com.test.project.drones.service;

import com.test.project.drones.model.drone.Battery;
import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.drone.StateDrone;

import java.util.List;


public interface DronesService {

    Drone createDrone(Drone drone);

    void changeStatus(Drone drone, StateDrone state);

    Drone getDroneBySerialNumber(String serialNumber);

    List<Drone> getAllDrones();

    void deleteDrone(String serialNumber);

    List<Drone> getFreeDrones();

    int getLoadedWeight(Drone drone);

    void sendForCharging(Drone drone);

    void decreaseBatteryLevel(Battery battery);
}
