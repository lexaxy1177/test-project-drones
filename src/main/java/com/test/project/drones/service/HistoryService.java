package com.test.project.drones.service;

import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.history.BatteryHistory;

import java.util.List;

public interface HistoryService {

    void createHistoryRecord(Drone drone);

    List<BatteryHistory> getBatteryLevelHistory(Drone drone);
}
