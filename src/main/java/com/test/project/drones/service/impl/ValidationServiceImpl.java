package com.test.project.drones.service.impl;

import com.test.project.drones.dto.OrderItemDto;
import com.test.project.drones.model.DroneConstants;
import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.drone.StateDrone;
import com.test.project.drones.model.medication.Medication;
import com.test.project.drones.model.order.Order;
import com.test.project.drones.service.OrderService;
import com.test.project.drones.service.ValidationService;
import com.test.project.drones.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class ValidationServiceImpl implements ValidationService {

    @Autowired
    OrderService orderService;

    @Autowired
    WarehouseService warehouseService;

    @Override
    public void checkDroneWeightLimit(Drone drone, Order order) {
        if (drone.getWeightLimit() < orderService.getOrderWeight(order)) {
            throw new RuntimeException("Drone has weightLimit less than Order weight");
        }
    }

    @Override
    public void checkAvailableToLoad(Drone drone) {
        if (drone.getBatteryLevel() <= DroneConstants.MIN_BATTERY_LEVEL) {
            throw new RuntimeException("Drone has low level of battery");
        }

        if (!StateDrone.IDLE.equals(drone.getState())) {
            throw new RuntimeException("Drone has status is different from IDLE");
        }
    }

    @Override
    public void checkWeightLimitBeforeAdd(Order order, List<OrderItemDto> itemsDto) {
        int sumWeightItems = 0;
        int maxWeight = order.getDrone() == null ? DroneConstants.MAX_WEIGHT
                : order.getDrone().getWeightLimit();

        for (OrderItemDto dto : itemsDto) {
            Medication medication = warehouseService.getMedicationByCode(dto.getCode());
            sumWeightItems += dto.getQuantity() * medication.getWeight();
        }

        if (sumWeightItems + orderService.getOrderWeight(order) > maxWeight) {
            throw new RuntimeException("No possible to add these medications because weight limit exceeded");
        }
    }

    @Override
    public void checkFormatOfInputValue(String str, Pattern pattern) {
        if (!pattern.matcher(str).matches()) {
            throw new RuntimeException("Entered incorrect format of value");
        }
    }
}
