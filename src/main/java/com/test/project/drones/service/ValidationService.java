package com.test.project.drones.service;

import com.test.project.drones.dto.OrderItemDto;
import com.test.project.drones.model.drone.Drone;
import com.test.project.drones.model.order.Order;

import java.util.List;
import java.util.regex.Pattern;

public interface ValidationService {

    void checkDroneWeightLimit(Drone drone, Order order);

    void checkAvailableToLoad(Drone drone);

    void checkWeightLimitBeforeAdd(Order order, List<OrderItemDto> itemsDto);

    void checkFormatOfInputValue(String str, Pattern pattern);
}
