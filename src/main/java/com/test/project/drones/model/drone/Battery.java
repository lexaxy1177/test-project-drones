package com.test.project.drones.model.drone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;

@Entity
@Table(name = "batteries")
public class Battery {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "battery_seq")
    @SequenceGenerator(name="battery_seq", sequenceName="SEQ_BATTERY", allocationSize=20)
    private long id;

    @Min(value = 0)
    @Max(value = 100)
    @Column(name = "capacity")
    private int capacity;

    @Column(name = "issue_date")
    private final Date issueDate;

    public Battery() {
        this.capacity = 100;
        this.issueDate = new Date();
    }

    public long getId() {
        return id;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Date getIssueDate() {
        return issueDate;
    }
}
