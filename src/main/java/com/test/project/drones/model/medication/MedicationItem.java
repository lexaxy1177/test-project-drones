package com.test.project.drones.model.medication;

import com.test.project.drones.model.order.OrderItem;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "medication_items")
public class MedicationItem {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medication_seq")
    @SequenceGenerator(name="medication_seq", sequenceName="SEQ_MEDICATION", allocationSize=20)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private MedicationStatus status;

    @Column(name = "issue_date")
    private  Date issueDate;

    @Column(name = "expiry_date")
    private  Date expiryDate;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "medication_code", foreignKey = @ForeignKey(name = "fk_warehouse_medication_id"))
    private Medication medication;

    @OneToOne(mappedBy="medicationItem", fetch= FetchType.LAZY)
    private OrderItem orderItem;

    public MedicationItem (Medication medication, Date issueDate, Date expiryDate) {
        this.medication = medication;
        this.issueDate = issueDate;
        this.expiryDate = expiryDate;
        this.status = MedicationStatus.AVAILABLE;
    }

    public MedicationItem(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public MedicationStatus getStatus() {
        return status;
    }

    public void setStatus(MedicationStatus status) {
        this.status = status;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }
}
