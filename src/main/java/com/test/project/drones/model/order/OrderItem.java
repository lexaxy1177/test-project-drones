package com.test.project.drones.model.order;

import com.test.project.drones.model.medication.MedicationItem;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "order_items")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_item_seq")
    @SequenceGenerator(name="order_item_seq", sequenceName="SEQ_ORDER_ITEM", allocationSize=20)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", foreignKey = @ForeignKey(name = "fk_orderitems_order_id"))
    private Order order;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "medication_item_id", foreignKey = @ForeignKey(name = "fk_orderitems_medication_item_id"))
    private MedicationItem medicationItem;

    public OrderItem(Order order, MedicationItem medicationItem) {
        this.order = order;
        this.medicationItem = medicationItem;
    }

    public OrderItem(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public OrderItem(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public MedicationItem getMedicationItem() {
        return medicationItem;
    }

    public void setMedicationItem(MedicationItem medicationItem) {
        this.medicationItem = medicationItem;
    }
}
