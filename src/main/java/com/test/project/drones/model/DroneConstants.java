package com.test.project.drones.model;

import java.util.regex.Pattern;

public interface DroneConstants {

    int MAX_WEIGHT = 500;
    int MIN_BATTERY_LEVEL = 25;

    Pattern MEDICATION_CODE_PATTERNS = Pattern.compile("[A-Z0-9_]*");
    Pattern MEDICATION_NAME_PATTERNS = Pattern.compile("[a-zA-Z0-9_-]*");
}
