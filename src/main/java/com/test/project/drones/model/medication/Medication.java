package com.test.project.drones.model.medication;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Positive;
import java.util.Collection;

@Entity
@Table(name = "medications")
public class Medication {

    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Positive
    @Column(name = "weight")
    private int weight;

    @OneToMany(mappedBy="medication", fetch= FetchType.LAZY)
    private Collection<MedicationItem> medicationItems;

    @Column(name = "img_path")
    private String imgPath;

    public Medication(String code, String name, int weight) {
        this.code = code;
        this.name = name;
        this.weight = weight;
    }

    public Medication(){}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Collection<MedicationItem> getMedicationItems() {
        return medicationItems;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }
}
