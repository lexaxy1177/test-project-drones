package com.test.project.drones.model.drone;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ModelDrone {

    LIGHTWEIGHT("Lightweight", 100),
    MIDDLEWEIGHT("Middleweight", 250),
    CRUISERWEIGHT("Cruiserweight", 400),
    HEAVYWEIGHT("Heavyweight", 500);

    private final String name;
    private final int weightLimit;

    ModelDrone(String model, int weightLimit) {
        this.name = model;
        this.weightLimit = weightLimit;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public int getWeightLimit() {
        return weightLimit;
    }

    public static ModelDrone getModelByName(String name) {
        for (ModelDrone model : ModelDrone.values()) {
            if (model.name.equalsIgnoreCase(name)) {
                return model;
            }
        }
        throw new RuntimeException("ModelDrone was not found by this name:" + name);
    }
}
