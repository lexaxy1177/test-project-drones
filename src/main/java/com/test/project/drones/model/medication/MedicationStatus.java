package com.test.project.drones.model.medication;

public enum MedicationStatus {
    PENDING,
    AVAILABLE,
    RESERVED,
    SOLD
}
