package com.test.project.drones.model.drone;

import com.test.project.drones.model.history.BatteryHistory;
import com.test.project.drones.model.order.Order;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "drones")
public class Drone {

    @Id
    @Column(name = "serial_number")
    @Size(max=100)
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "model")
    private ModelDrone model;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "battery_id", foreignKey = @ForeignKey(name = "fk_drones_battery_id"))
    private Battery battery;

    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private StateDrone state;

    @OneToOne(mappedBy = "drone", fetch = FetchType.LAZY)
    private Order order;

    @OneToMany(mappedBy="drone", fetch= FetchType.LAZY)
    private List<BatteryHistory> batteryHistory;

    public Drone(String serialNumber, String modelName) {
        this.serialNumber = serialNumber;
        this.model = ModelDrone.getModelByName(modelName);
        this.state = StateDrone.IDLE;
        this.battery = new Battery();
    }

    public Drone(){}

    public int getWeightLimit() {
        return getModel().getWeightLimit();
    }

    public int getBatteryLevel() {
        return getBattery().getCapacity();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public ModelDrone getModel() {
        return model;
    }

    public void setModel(ModelDrone model) {
        this.model = model;
    }

    public Battery getBattery() {
        return battery;
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }

    public StateDrone getState() {
        return state;
    }

    public void setState(StateDrone state) {
        this.state = state;
    }

    public Order getOrder() {
        return order;
    }

    public List<BatteryHistory> getBatteryHistory() {
        return batteryHistory;
    }
}
