package com.test.project.drones.model.drone;

public enum StateDrone {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
}
