package com.test.project.drones.model.order;

public enum OrderStatus {
    NEW,
    WAITING_DELIVERY,
    DRONE_ASSIGNED,
    COMPLETED,
    IN_DELIVERY,
    DELIVERED,
    CANCELED
}
