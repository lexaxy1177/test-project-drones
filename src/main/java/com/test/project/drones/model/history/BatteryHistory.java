package com.test.project.drones.model.history;

import com.test.project.drones.model.drone.Drone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "history_battery")
public class BatteryHistory {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "battery_history_seq"
    )
    @SequenceGenerator(
            name="battery_history_seq",
            sequenceName="SEQ_HISTORY_BATTERY",
            allocationSize=20)
    private long id;

    @Column(name = "date")
    private Date date;

    @ManyToOne()
    @JoinColumn(name = "drone_id", foreignKey = @ForeignKey(name = "fk_history_drone_id"))
    private Drone drone;

    @Column(name = "level")
    private int batteryLevel;

    public BatteryHistory(Drone drone) {
        this.date = new Date();
        this.drone = drone;
        this.batteryLevel = drone.getBatteryLevel();
    }

    public BatteryHistory(){}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
}
