package com.test.project.drones.repository;

import com.test.project.drones.model.drone.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DronesRepository extends JpaRepository<Drone, String> {
}
