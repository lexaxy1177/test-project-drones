package com.test.project.drones.repository;

import com.test.project.drones.model.history.BatteryHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BatteryHistoryRepository extends JpaRepository<BatteryHistory, String> {
}
