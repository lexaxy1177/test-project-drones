package com.test.project.drones.repository;

import com.test.project.drones.model.medication.MedicationItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationItemRepository extends JpaRepository<MedicationItem, Long> {
}