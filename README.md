# Test Project - Drones



# Introduction
application.yml - setting file
h2 database:
Database link:
http://localhost:8080/h2-console
Driver Class: org.h2.Driver
JDBC URL: jdbc:h2:file:./src/main/resources/data/drones_db
User Name: sa


Properties:
* {job.scheduler.enabled} - flag to enable or disable the job for control and store battery level for Drones  
* {job.scheduler.delay} - delay for job (default value - 1 min)

InitTestLoader class - loading test data


Rest Controller classes:
* DronesController
* OrdersController
* WarehouseController


# Intruction of building and runing
1) mvn spring-boot:run
2) mvn clean package spring-boot:repackage
3) java -jar drones-0.0.1-SNAPSHOT.jar




# REST APIs:
#####################
Drone Service:
#####################

1) Get all Drones
GET: http://localhost:8080/api/drones


2) Get Drone by {serialNumber}
GET: http://localhost:8080/api/drones/{serialNumber}

example: 
GET: http://localhost:8080/api/drones/DRON1


3) Create Drone
PUST: http://localhost:8080/api/drones/
Request example:
{
  "serialNumber": "DRON11",
  "model" : "Lightweight"
}


4) Delete Drone by {serialNumber}
DELETE: http://localhost:8080/api/drones/{serialNumber}

example: 
DELETE: http://localhost:8080/api/drones/DRON1



5) Send Drone to charging
PUT: http://localhost:8080/api/drones/charging/{serialNumber}

example:
PUT: http://localhost:8080/api/drones/charging/DRON1


6) Get all Free Drones (available to load)
GET: http://localhost:8080/api/drones/free


7) Get history of battery level for Drone with {serialNumber}
GET: http://localhost:8080/api/drones/history/battery/{serialNumber}

example:
GET: http://localhost:8080/api/drones/history/battery/DRON1




#####################
Warehouse Service:
#####################

1) Get available Medications in Warehouse and their quantity:  
GET: http://localhost:8080/api/warehouse/available


2) Get Image for Medication by code:
http://localhost:8080/api/warehouse/image/{code}

example:
http://localhost:8080/api/warehouse/image/MEDICATION1




#####################
Order Service:
#####################
1) Get all Orders
GET: http://localhost:8080/api/orders


2) Get Order by {id}
GET: http://localhost:8080/api/orders/{id}

example: 
GET: http://localhost:8080/api/orders/1


3) Create Order
POST: http://localhost:8080/api/orders

Request example:
{
  "deliveryDate": "2022-12-20T12:22:17.238+0000",
  "deliveryAddress" : "TEST address 3"
}



4) Add Medications to Order {id}
POST: http://localhost:8080/api/orders/{id}/addMedication

Example:
POST: http://localhost:8080/api/orders/2/addMedication

Request:
[
  {
  	"code": "MEDICATION1",
  	"quantity": 2
  }
]


5) Assign Drone with {serialNumber} to Order with {id}
PUT: http://localhost:8080/api/orders/{id}/assign/{serialNumber}

example:
PUT: http://localhost:8080/api/orders/2/assign/DRON1



6) Complete loading Order with {id} 
PUT: http://localhost:8080/api/orders/{id}/complete

example:
PUT: http://localhost:8080/api/orders/2/complete


7) Send to delivery loading Order with {id} 
PUT: http://localhost:8080/api/orders/{id}/delivery

example:
PUT: http://localhost:8080/api/orders/2/delivery


8) Cancel Order with {id} (all related Medications and Drone will be released)
PUT: http://localhost:8080/api/orders/{id}/cancel

example:
PUT: http://localhost:8080/api/orders/2/cancel



test pipeline